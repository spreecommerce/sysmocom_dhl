class AddDhlCouponTable < ActiveRecord::Migration
  def self.up
    create_table :sysmocom_dhl_coupons do |t|
      t.timestamps
      t.references :order
      t.string :type
      t.string :code
    end
  end

  def self.down
    drop_table :sysmocom_dhl_coupons
  end
end
