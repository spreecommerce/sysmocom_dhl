class DhlShippingMigration < ActiveRecord::Migration
  def self.up
     change_column :spree_shipping_methods, :method_details, :text
  end

  def self.down
     change_column :spree_shipping_methods, :method_details, :string
  end
end
