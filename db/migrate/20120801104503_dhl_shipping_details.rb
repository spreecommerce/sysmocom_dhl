class DhlShippingDetails < ActiveRecord::Migration
  def self.up
      add_column :spree_shipping_methods, :method_details, :string
  end

  def self.down
      remove_column :spree_shipping_methods, :method_details
  end
end
