Spree::Admin::ShipmentsController.class_eval do
  def dhllabel
    name = '%s_dhllabel.csv' % [order.number]
    headers["Content-type"] = "text/plain; charset=iso-8859-15"
    headers["Cache-Control"] = "no-cache, max-age=0, must-revalidate"
    headers["Pragma"] = "no-cache"
    headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    headers["Content-Disposition"] = "attachment; filename=\"%s\"" % name

    # Try hard to render it as latin1
    res = render :locals => { :shipment => shipment }
    response.charset = 'iso-8859-15'
    response.body = Iconv.conv('iso-8859-15', 'utf-8', res[0])
  end
end
