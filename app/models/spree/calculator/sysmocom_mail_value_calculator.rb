# AGPLv3 sysmocom s.f.m.c. GmbH

module Spree
  class Calculator::SysmocomMailValueCalculator < Calculator

    def self.description
      I18n.t(:sysmocom_mail_value_based)
    end

    def total_value_no_tax(o)
        item_total = o.line_items.map(&:amount).sum
        item_total
    end

    def total_weight(o)
	weight = 0
	o.line_items.each do |li|
          weight += li.quantity * li.variant.weight
        end
        return weight
    end

    def extract_iso_code(o)
        if o.kind_of?(Spree::Shipment)
            return o.address.country.iso
        end
        return o.ship_address.country.iso
    end


    def postage_for_weight_DE(weight)
        # Maxibrief Einschreiben: 5.50
        if weight < 1000
          return 5.50
	else
	  return 99999
        end
    end

    def postage_for_weight_INTL(weight)
        # Warenpost International Unterschrift S: 8.50 / M: 12.50 / L: 23.40
        if weight < 500
          return 8.50
        elsif weight < 1000
          return 12.50
        elsif weight < 2000
          return 23.40
	else
	  return 99999
        end
    end

    def self.available?(object)
        iso_code = extract_iso_code(object)
        weight = total_weight(object)
        if iso_code == 'DE'
	  if weight < 1000
            return true
	  else
            return false
          end
	else
          # Letters have a maximum weight of 2kg
          if weight < 2000
            return true
          else
            return false
          end
	end
    end

    def compute(object)
        # This could be more than an order but not right now
        shipping_value = total_value_no_tax(object)
        weight = total_weight(object)
        iso_code = extract_iso_code(object)

        if iso_code == 'DE'
          base = postage_for_weight_DE(weight)
        else
          base = postage_for_weight_INTL(weight)
          # We need to add insurance..
          if shipping_value > 20
            # Wert Intenational: 2.50 + 2 EUR je 100 EUR Haftungssumme
            base += 2.5 + (((shipping_value.round / 100) + 1) * 2)
          end
        end
        return base
    end

  end
end


