Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "dhl_admin_tabs",
                     :insert_bottom => "[data-hook='admin_tabs'], #admin_tabs[data-hook]",
                     :text => "<%= tab(:dhls, :url => spree.admin_dhls_path) %>",
                     :disabled => false,
        	     :original => 'b06db401e133fa2491f542e73196e0bb79a4ca05')

Deface::Override.new(:virtual_path => "spree/admin/shipments/edit",
        	     :name => "dhl_label",
                     :insert_bottom => "[data-hook='admin_shipment_edit_buttons']",
        	     :partial => 'spree/admin/dhls/address_label',
                     :original => 'e5254ea08fdafd6dfaf37a2631fdc384f0a4a3ae')

Deface::Override.new(:virtual_path => "spree/admin/shipping_methods/_form",
                     :name => 'dhl_shipment_method',
                     :insert_bottom => "[data-hook='admin_shipping_method_form_fields']",
                     :partial => 'spree/admin/dhls/method_details_form')


# This doesn't work with the shipment mailer..
