Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'sysmocom_dhl'
  s.version     = '2.0.0'
  s.summary     = 'sysmocom_dhl shipping helper'
  s.description = 'Deal with shipping cost, zones, insurance'
  s.required_ruby_version = '>= 1.9.1'

  s.author	= 'Holger Freyther'
  # s.author            = 'David Heinemeier Hansson'
  # s.email             = 'david@loudthinking.com'
  # s.homepage          = 'http://www.rubyonrails.org'
  # s.rubyforge_project = 'actionmailer'

  #s.files         = `git ls-files`.split("\n")
  #s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency('spree_core', '>= 1.2.0')
  s.add_dependency('shipcloud')

  s.add_development_dependency 'capybara', '1.0.1'
  s.add_development_dependency 'factory_girl', '~> 2.6.4'
  s.add_development_dependency 'ffaker'
  s.add_development_dependency 'rspec-rails',  '~> 2.9'
  s.add_development_dependency 'sqlite3'
end
