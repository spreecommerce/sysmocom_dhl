module SysmocomDhl
  class Engine < Rails::Engine
    require 'spree/core'
    isolate_namespace Spree
    engine_name 'sysmocom_dhl'

    config.autoload_paths += %W(#{config.root}/lib)

    # use rspec for tests
    config.generators do |g|
      g.test_framework :rspec
    end

    def self.activate
      Dir.glob(File.join(File.dirname(__FILE__), '../../app/**/*_decorator*.rb')) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end
    end

    initializer "sysmocom_shipping_calculators" do |app|
      app.config.spree.calculators.shipping_methods += [
         # ActiveSupport magic to search a model...
         Spree::Calculator::SysmocomValueCalculator,
         Spree::Calculator::SysmocomMailValueCalculator,
         Spree::Calculator::SysmocomScUpsStdCalculator,
         Spree::Calculator::SysmocomScUpsExpressCalculator,
         Spree::Calculator::SysmocomScUpsExpeditedCalculator,
      ]
    end

    config.to_prepare &method(:activate).to_proc
  end
end
