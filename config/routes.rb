Spree::Core::Engine.routes.draw do
  # Add your extension routes here
  namespace :admin do
    resources :dhls do
      resources :coupons
    end

    # Add access to the dhllabel function
    resources :orders do
      resources :shipments do
        member do
          get :dhllabel
        end
      end
    end
  end


  match "/admin/dhl" => 'admin/dhls#index', :as => :admin
end
